package com.perpustakaan.web.controller;

import java.sql.Date;
import java.time.Instant;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.data.domain.Page;
import org.springframework.web.bind.annotation.RestController;

import com.perpustakaan.web.model.Book;
import com.perpustakaan.web.model.Genre;
import com.perpustakaan.web.repository.BookRepository;

@RestController
@CrossOrigin("*")
@RequestMapping("/api/")
public class ApiBookController {

	@Autowired
	public BookRepository bookRepository;

	@GetMapping("book")
	public ResponseEntity<Object> showBook(@RequestParam(defaultValue = "0") int page) {
		List<Book> listBook = bookRepository.findAll();
		
		Comparator<Book> comparator = Comparator.comparing(Book::getBookCode);
		int pageSize = 3;
		
		Long totalData = listBook.stream().count();

		Long totalPage = totalData / pageSize;
		Long addDataPage = totalData % pageSize;
		if (addDataPage > 0) {
			totalPage += 1;
		}

		if (page > 0) {
			List<Book> listBookCode = listBook.stream()
					.filter(result -> result.getIsDelete() == false)
					.sorted(comparator)
					.skip(page * 3)
					.limit(3)
					.collect(Collectors.toList());
			if (listBookCode.isEmpty()) {
				return new ResponseEntity<>(HttpStatus.NO_CONTENT);
			} else {
				Map<String, Object> response = new HashMap<>();
				response.put("list", listBookCode);
				response.put("currentPage", (page + 1));
				response.put("totalItem", totalData);
				response.put("totalPage", totalPage);
				return new ResponseEntity<>(response, HttpStatus.OK);
			}
		} else {
			List<Book> listBookCode = listBook.stream()
					.filter(result -> result.getIsDelete() == false)
					.sorted(comparator)
					.limit(3)
					.collect(Collectors.toList());
			if (listBookCode.isEmpty()) {
				return new ResponseEntity<>(HttpStatus.NO_CONTENT);
			} else {
				Map<String, Object> response = new HashMap<>();
				response.put("list", listBookCode);
				response.put("currentPage", (page + 1));
				response.put("totalItem", totalData);
				response.put("totalPage", totalPage);
				return new ResponseEntity<>(response, HttpStatus.OK);
			}
		}
	}

	@GetMapping("sort/bookName")
	public ResponseEntity<Object> showBookByBookName(@RequestParam(defaultValue = "0") int page) {
		List<Book> listBook = bookRepository.findAll();

		// inisiasi sorting agar memudahkan ketika maintenance
		Comparator<Book> comparator = Comparator.comparing(Book::getBookName);

		Long totalData = listBook.stream().count();

		Long totalPage = totalData / 3;
		Long addDataPage = totalData % 3;
		if (addDataPage > 0) {
			totalPage += 1;
		}

		if (page > 0) {
			List<Book> listBookName = listBook.stream()
					.filter(result -> result.getIsDelete() == false)
					.sorted(comparator)
					.skip(page * 3)
					.limit(3)
					.collect(Collectors.toList());
			if (listBookName.isEmpty()) {
				return new ResponseEntity<>(HttpStatus.NO_CONTENT);
			} else {
				Map<String, Object> response = new HashMap<>();
				response.put("list", listBookName);
				response.put("currentPage", (page + 1));
				response.put("totalItem", totalData);
				response.put("totalPage", totalPage);
				return new ResponseEntity<>(response, HttpStatus.OK);
			}
		} else {
			List<Book> listBookName = listBook.stream()
					.filter(result -> result.getIsDelete() == false)
					.sorted(comparator)
					.limit(3)
					.collect(Collectors.toList());
			if (listBookName.isEmpty()) {
				return new ResponseEntity<>(HttpStatus.NO_CONTENT);
			} else {
				Map<String, Object> response = new HashMap<>();
				response.put("list", listBookName);
				response.put("currentPage", (page + 1));
				response.put("totalItem", totalData);
				response.put("totalPage", totalPage);
				return new ResponseEntity<>(response, HttpStatus.OK);
			}
		}
	}

	@GetMapping("sort/authorName")
	public ResponseEntity<Object> showBookByAuthorName(@RequestParam(defaultValue = "0") int page) {
		List<Book> listBook = bookRepository.findAll();

		Comparator<Book> comparator = Comparator.comparing(Book::getAuthorName);

		Long totalData = listBook.stream().count();

		Long totalPage = totalData / 3;
		Long addDataPage = totalData % 3;
		if (addDataPage > 0) {
			totalPage += 1;
		}
		
		if (page > 0) {
			List<Book> listAuthorName = listBook.stream()
					.filter(result -> result.getIsDelete() == false)
					.sorted(comparator)
					.skip(page * 3)
					.limit(3)
					.collect(Collectors.toList());
			if (listAuthorName.isEmpty()) {
				return new ResponseEntity<>(HttpStatus.NO_CONTENT);
			} else {
				Map<String, Object> response = new HashMap<>();
				response.put("list", listAuthorName);
				response.put("currentPage", (page + 1));
				response.put("totalItem", totalData);
				response.put("totalPage", totalPage);
				return new ResponseEntity<>(response, HttpStatus.OK);
			}
		} else {
			List<Book> listAuthorName = listBook.stream()
					.filter(result -> result.getIsDelete() == false)
					.sorted(comparator)
					.limit(3)
					.collect(Collectors.toList());
			if (listAuthorName.isEmpty()) {
				return new ResponseEntity<>(HttpStatus.NO_CONTENT);
			} else {
				Map<String, Object> response = new HashMap<>();
				response.put("list", listAuthorName);
				response.put("currentPage", (page + 1));
				response.put("totalItem", totalData);
				response.put("totalPage", totalPage);
				return new ResponseEntity<>(response, HttpStatus.OK);
			}
		}
	}

	@GetMapping("sort/stock")
	public ResponseEntity<Object> showBookByStock(@RequestParam(defaultValue = "0") int page) {
		List<Book> listBook = bookRepository.findAll();

		Comparator<Book> comparator = Comparator.comparingInt(Book::getStok);
		
		Long totalData = listBook.stream().count();

		Long totalPage = totalData / 3;
		Long addDataPage = totalData % 3;
		if (addDataPage > 0) {
			totalPage += 1;
		}

		if (page > 0) {
			List<Book> listBookStock = listBook.stream()
					.filter(result -> result.getIsDelete() == false)
					.sorted(comparator)
					.skip(page * 3)
					.limit(3)
					.collect(Collectors.toList());
			if (listBookStock.isEmpty()) {
				return new ResponseEntity<>(HttpStatus.NO_CONTENT);
			} else {
				Map<String, Object> response = new HashMap<>();
				response.put("list", listBookStock);
				response.put("currentPage", (page + 1));
				response.put("totalItem", totalData);
				response.put("totalPage", totalPage);
				return new ResponseEntity<>(response, HttpStatus.OK);
			}
		} else {
			List<Book> listBookStock = listBook.stream()

					// filter 2 kondisi dan mencari nama buku yang mengandung huruf K
					// .filter(result -> result.getIsDelete() == false &&
					// result.getBookName().toUpperCase().contains("K"))

					// filter 1 kondisi
					.filter(result -> result.getIsDelete() == false)
					// sorting asc
					.sorted(comparator.reversed())
					.limit(3)

					// sorting desc
					// .sorted(comparator.reversed())
					.collect(Collectors.toList());

			if (listBookStock.isEmpty()) {
				return new ResponseEntity<>(HttpStatus.NO_CONTENT);
			} else {
				Map<String, Object> response = new HashMap<>();
				response.put("list", listBookStock);
				response.put("currentPage", (page + 1));
				response.put("totalItem", totalData);
				response.put("totalPage", totalPage);
				return new ResponseEntity<>(response, HttpStatus.OK);
			}	
		}
	}

	@PostMapping("add/book")
	public ResponseEntity<Object> addBook(@RequestBody Book book) {

		book.setCreatedOn(Date.from(Instant.now()));
		book.setIsDelete(false);

		Book bookData = this.bookRepository.save(book);

		if (bookData.equals(book)) {
			return new ResponseEntity<Object>("Save Data Successfully", HttpStatus.OK);
		} else {
			return new ResponseEntity<Object>("Save Failed", HttpStatus.BAD_REQUEST);
		}
	}

	@PutMapping("edit/book/{id}")
	public ResponseEntity<Object> replaceBook(@PathVariable Long id, @RequestBody Book newBook) {

		Optional<Book> bookData = this.bookRepository.findById(id);

		return bookRepository.findById(id).map(Book -> {
			Book.setId(id);
			Book.setModifyOn(Date.from(Instant.now()));
			Book.setCreatedBy(bookData.get().getCreatedBy());
			Book.setCreatedOn(bookData.get().getCreatedOn());
			Book.setIsDelete(bookData.get().getIsDelete());

			Book.setModifyBy(newBook.getModifyBy());
			Book.setAuthorName(newBook.getAuthorName());
			Book.setStok(bookData.get().getStok() + newBook.getStok());
			Book.setBookCode(newBook.getBookCode());
			Book.setBookName(newBook.getBookName());
			Book.setGenreId(newBook.getGenreId());

			this.bookRepository.save(Book);
			return new ResponseEntity<Object>("Updated Successfully", HttpStatus.OK);
		}).orElseGet(() -> {
			return ResponseEntity.notFound().build();
		});
	}

	@PutMapping("delete/book/{id}")
	public ResponseEntity<Object> deleteBook(@RequestBody Book newBook, @PathVariable Long id) {
		Optional<Book> bookData = this.bookRepository.findById(id);

		return bookRepository.findById(id).map(Book -> {
			Book.setId(id);
			Book.setModifyOn(bookData.get().getModifyOn());
			Book.setModifyBy(bookData.get().getModifyBy());
			Book.setCreatedBy(bookData.get().getCreatedBy());
			Book.setCreatedOn(bookData.get().getCreatedOn());
			Book.setDeletedOn(Date.from(Instant.now()));
			Book.setIsDelete(true);

			Book.setDeletedBy(newBook.getDeletedBy());
			Book.setAuthorName(newBook.getAuthorName());
			Book.setStok(newBook.getStok());
			Book.setBookCode(newBook.getBookCode());
			Book.setBookName(newBook.getBookName());
			Book.setGenreId(newBook.getGenreId());

			this.bookRepository.save(Book);
			return new ResponseEntity<Object>("Updated Successfully", HttpStatus.OK);
		}).orElseGet(() -> {
			return ResponseEntity.notFound().build();
		});
	}

	@GetMapping("getBookCode/")
	public ResponseEntity<Optional<Book>> getBookByCode(@RequestParam("bookCode") String bookCode) {
		List<Book> book = this.bookRepository.findAll();

		Optional<Book> listBookCode = book.stream()
				.filter(result -> result.getBookCode().equals(bookCode))
				.findFirst();

		return new ResponseEntity<Optional<Book>>(listBookCode, HttpStatus.OK);
	}

	@GetMapping("getBookName/")
	public ResponseEntity<Optional<Book>> getBookByName(@RequestParam("bookName") String bookName) {
		List<Book> book = this.bookRepository.findAll();

		Optional<Book> listBookName = book.stream()
				.filter(result -> result.getBookName().equals(bookName))
				.findFirst();

		return new ResponseEntity<Optional<Book>>(listBookName, HttpStatus.OK);
	}

	@GetMapping("book/{id}")
	public ResponseEntity<List<Book>> getBookById(@PathVariable("id") Long id) {
		List<Book> book = this.bookRepository.findAll();
		List<Book> listBookId = book.stream()
				.filter(result -> result.getId().equals(id))
				.collect(Collectors.toList());

		if (listBookId.isEmpty()) {
			return ResponseEntity.notFound().build();
		} else {
			return new ResponseEntity<List<Book>>(listBookId, HttpStatus.OK);
		}
	}

	@PostMapping("bookName/search/")
	public ResponseEntity<Optional<Book>> getBookBySearch(@RequestParam("keyword") String keyword) {
		if (!keyword.equals("")) {

			List<Book> book = this.bookRepository.findAll();

			Optional<Book> listBook = book.stream()
					// .filter(result -> result.getBookName().equals(keyword) &&
					// result.getBookName().startsWith("A"))
					.filter(result -> result.getBookName().equals(keyword))
					.findFirst();
			return new ResponseEntity<Optional<Book>>(listBook, HttpStatus.OK);
		} else {
			return new ResponseEntity<Optional<Book>>(HttpStatus.NO_CONTENT);
		}
	}
}
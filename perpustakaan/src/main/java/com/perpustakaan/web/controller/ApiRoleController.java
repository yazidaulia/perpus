package com.perpustakaan.web.controller;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.perpustakaan.web.model.Role;
import com.perpustakaan.web.repository.RoleRepository;


@RestController
@CrossOrigin("*")
@RequestMapping("/api/")
public class ApiRoleController {
	
	@Autowired
	public RoleRepository roleRepository;
	
	@GetMapping("role")
	public ResponseEntity<List<Role>> getRoleActive() {
		try {
			List<Role> roleList = this.roleRepository.findByIsDelete(false);
			return new ResponseEntity<>(roleList, HttpStatus.OK);
		} catch (Exception e) {
			return new ResponseEntity<>(HttpStatus.NO_CONTENT);
		}
	}
	
	@GetMapping("role/pagingfilteringsorting")
	public ResponseEntity<Map<String, Object>> getAllRolePageAndFilter(
			@RequestParam(defaultValue = "") String roleName,
			@RequestParam(defaultValue = "0") int page,
			@RequestParam(defaultValue = "3") int size) {
		try {
			List<Role> role = new ArrayList<>();
			Pageable pageSelect = PageRequest.of(page, size);
			Page<Role> pageContent;
			pageContent = this.roleRepository.searchByRoleName(roleName, pageSelect);
			role = pageContent.getContent();
			Map<String, Object> response = new HashMap<>();
			response.put("roleName", role);
			response.put("currentPage", pageContent.getNumber());
			response.put("totalItem", pageContent.getTotalElements());
			response.put("totalPage", pageContent.getTotalPages());
			return new ResponseEntity<>(response, HttpStatus.OK);
		} catch (Exception e) {
			return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}
	
	@PostMapping("roleName/search/")
	public ResponseEntity<List<Role>> getRoleByName(@RequestParam("keyword") String keyword){
		if(keyword.equals("")){
			List<Role> role = this.roleRepository.findByIsDelete(false);
			return new ResponseEntity<List<Role>>(role, HttpStatus.OK);
		} else {
			List<Role> role = this.roleRepository.searchByKeyword(keyword);
			return new ResponseEntity<List<Role>>(role, HttpStatus.OK);
		}
	}
}
package com.perpustakaan.web.controller;

import java.sql.Date;
import java.time.Instant;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.perpustakaan.web.model.Genre;
import com.perpustakaan.web.repository.GenreRepository;

@RestController
@CrossOrigin("*")
@RequestMapping("/api/")
public class ApiGenreController {
	@Autowired
	public GenreRepository genreRepository;
	
	@GetMapping("genre")
	public ResponseEntity<List<Genre>> getGenreActive() {
		try {
			List<Genre> genreList = this.genreRepository.findByIsDeleteOrderByGenreNameAsc(false);
			return new ResponseEntity<>(genreList, HttpStatus.OK);
		} catch (Exception e) {
			return new ResponseEntity<>(HttpStatus.NO_CONTENT);
		}
	}
	
	@PostMapping("add/genre")
	public ResponseEntity<Object> saveGenre(@RequestBody Genre genre){
		
		genre.setCreatedOn(Date.from(Instant.now()));
		genre.setIsDelete(false);
		
		Genre genreData = this.genreRepository.save(genre);
		
		if(genreData.equals(genre)) {
			return new ResponseEntity<Object>("Save Data Successfully", HttpStatus.OK);
		}else {
			return new ResponseEntity<Object>("Save Failed", HttpStatus.BAD_REQUEST);
		}
	}
	
	@PostMapping("genreName/search/")
	public ResponseEntity<List<Genre>> getGenreByName(@RequestParam("keyword") String keyword){
		if(keyword.equals("")){
			List<Genre> genre = this.genreRepository.findByIsDeleteOrderByGenreNameAsc(false);
			return new ResponseEntity<List<Genre>>(genre, HttpStatus.OK);
		} else {
			List<Genre> genre = this.genreRepository.searchByKeyword(keyword);
			return new ResponseEntity<List<Genre>>(genre, HttpStatus.OK);
		}
	}
	
	//paging and searching
	@PostMapping("genreName/PagingSearch")
	public ResponseEntity<Map<String, Object>> getAllGenreNameWithPaging(
			@RequestParam(defaultValue = "") String genreName,
			@RequestParam(defaultValue = "0") int page,
			@RequestParam(defaultValue = "2") int size) {
		try {
			List<Genre> genree = new ArrayList<>();
			Pageable pageSelect = PageRequest.of(page, size);
			Page<Genre> pageContent;
			pageContent = this.genreRepository.searchByGenreName(genreName, pageSelect);
			genree = pageContent.getContent();
			Map<String, Object> response = new HashMap<>();
			response.put("genreName", genree);
			response.put("currentPage", pageContent.getNumber());
			response.put("totalItem", pageContent.getTotalElements());
			response.put("totalPage", pageContent.getTotalPages());
			return new ResponseEntity<>(response, HttpStatus.OK);
		} catch (Exception e) {
			return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}
	
	@GetMapping("get_genre/")
	public ResponseEntity<List<Genre>> getGenreByCode(@RequestParam("genreCode") String genreCode) {
		List<Genre> genre = this.genreRepository.findByGenreCode(genreCode);
		return new ResponseEntity<List<Genre>>(genre, HttpStatus.OK);
	}
	
	@GetMapping("getGenre/")
	public ResponseEntity<List<Genre>> getGenreBygenreName(@RequestParam("genreName") String genreName) {
		List<Genre> genre = this.genreRepository.findByGenreName(genreName);
		return new ResponseEntity<List<Genre>>(genre, HttpStatus.OK);
	}
	
	@GetMapping("genre/{id}")
	public ResponseEntity<List<Genre>> getGenreById(@PathVariable("id") Long id) {
		try {
			Optional<Genre> genre = this.genreRepository.findById(id);
			if(genre.isPresent()) {
				ResponseEntity rest = new ResponseEntity<>(genre, HttpStatus.OK);
				return rest;
			}else {
				return ResponseEntity.notFound().build();
			}
		} catch(Exception e) {
			return new ResponseEntity<List<Genre>>(HttpStatus.NO_CONTENT);
		}
	}
	
	@PutMapping("edit/genre/{id}")
	public ResponseEntity<Object> editGenre(@PathVariable("id") Long id,
			@RequestBody Genre genre){
		Optional<Genre> genreData = this.genreRepository.findById(id);
		if(genreData.isPresent()) {
			genre.setId(id);
			genre.setModifyOn(Date.from(Instant.now()));
			genre.setCreatedBy(genreData.get().getCreatedBy());
			genre.setCreatedOn(genreData.get().getCreatedOn());
			genre.setIsDelete(genreData.get().getIsDelete());
			
			this.genreRepository.save(genre);
			return new ResponseEntity<Object>("Updated Successfully", HttpStatus.OK);
		}else {
			return ResponseEntity.notFound().build();
		}
	}
	
	@PutMapping("delete/genre/{id}")
	public ResponseEntity<Object> deleteGenre(@PathVariable("id") Long id, @RequestBody Genre genre){
		Optional<Genre> genreData = this.genreRepository.findById(id);
		
		if(genreData.isPresent()) {
			genre.setId(id);
			genre.setIsDelete(true);
			genre.setModifyOn(Date.from(Instant.now()));
			genre.setCreatedBy(genreData.get().getCreatedBy());
			genre.setCreatedOn(genreData.get().getCreatedOn());
			genre.setGenreCode(genreData.get().getGenreCode());
			genre.setGenreName(genreData.get().getGenreName());
			
			this.genreRepository.save(genre);
			return new ResponseEntity<Object>("Deleted Successfully", HttpStatus.OK);
		}else {
			return ResponseEntity.notFound().build();
		}
	}
}
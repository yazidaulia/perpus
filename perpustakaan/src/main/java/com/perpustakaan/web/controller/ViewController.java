package com.perpustakaan.web.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

@Controller
@RequestMapping("/")
public class ViewController {
	
	@GetMapping("login")
	public ModelAndView indexapi() {
		ModelAndView view = new ModelAndView("user/indexapi");
		return view;
	}

	@GetMapping("pendaftaran")
	public ModelAndView pendaftaranapi() {
		ModelAndView view = new ModelAndView("pendaftaran/pendaftaranapi");
		return view;
	}

	@GetMapping("resetpassword")
	public ModelAndView forgetpassword() {
		ModelAndView view = new ModelAndView("forgetpassword/forgetpassword");
		return view;
	}
	
	@GetMapping("genre")
	public ModelAndView genre() {
		ModelAndView view = new ModelAndView("genre/genre");
		return view;
	}
	
	@GetMapping("book")
	public ModelAndView book() {
		ModelAndView view = new ModelAndView("book/book");
		return view;
	}
}

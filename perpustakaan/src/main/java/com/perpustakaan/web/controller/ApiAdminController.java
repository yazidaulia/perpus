package com.perpustakaan.web.controller;

import java.time.Instant;
import java.util.Date;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.perpustakaan.web.model.Admin;
import com.perpustakaan.web.repository.AdminRepository;

@RestController
@CrossOrigin("*")
@RequestMapping("/api/")
public class ApiAdminController {
	@Autowired
	public AdminRepository adminRepository;
	
	@PostMapping("post/admin")
	public ResponseEntity<Object> saveAdmin(@RequestBody Admin admin){
		
		admin.setCreatedOn(Date.from(Instant.now()));
		
		Admin adminData = this.adminRepository.save(admin);
		
		if(adminData.equals(admin)) {
			return new ResponseEntity<Object>("Save Data Successfully", HttpStatus.OK);
		}else {
			return new ResponseEntity<Object>("Save Failed", HttpStatus.BAD_REQUEST);
		}
	}
}
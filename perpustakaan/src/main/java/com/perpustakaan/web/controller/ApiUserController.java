package com.perpustakaan.web.controller;

import java.time.Instant;
import java.util.Date;
import java.util.List;
import java.util.Optional;
import java.math.BigInteger;
import java.security.*;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.perpustakaan.web.model.User;
import com.perpustakaan.web.repository.UserRepository;

@RestController
@CrossOrigin("*")
@RequestMapping("/api/")
public class ApiUserController {

	@Autowired
	public UserRepository userRepository;

	//get all user by is locked = false
	@GetMapping("user")
	public ResponseEntity<List<User>> getAllUser() {
		try {
			List<User> user = this.userRepository.findByIsLocked(false);
			return new ResponseEntity<>(user, HttpStatus.OK);
		} catch (Exception e) {
			return new ResponseEntity<>(HttpStatus.NO_CONTENT);
		}
	}

	// Untuk mendapatkan email, return list agar bisa mendapatkan field lain sesuai
	// hasil yg didapat
	@GetMapping("user/")
	public ResponseEntity<List<User>> getUserByEmail(@RequestParam("email") String email) {
		List<User> user = this.userRepository.findByEmail(email);
		return new ResponseEntity<List<User>>(user, HttpStatus.OK);
	}

	// Untuk mengubah last login
	@PutMapping("edit/userlogin/{id}")
	public ResponseEntity<Object> editUserLogin(@PathVariable("id") Long id, @RequestBody User user) {
		Optional<User> userData = this.userRepository.findById(id);
		if (userData.isPresent()) {
			user.setId(userData.get().getId());
			user.setEmail(userData.get().getEmail());
			user.setPassword(userData.get().getPassword());
			user.setModifyOn(Date.from(Instant.now()));

			user.setCreatedBy(userData.get().getCreatedBy());
			user.setCreatedOn(userData.get().getCreatedOn());
			user.setBiodataId(userData.get().getBiodataId());
			user.setRoleId(userData.get().getRoleId());
			user.setDeletedBy(userData.get().getDeletedBy());
			user.setDeletedOn(userData.get().getDeletedOn());
			user.setIsDelete(userData.get().getIsDelete());
			user.setIsLocked(userData.get().getIsLocked());
			user.setLastLogin(Date.from(Instant.now()));
			user.setLoginAttempt(0);

			this.userRepository.save(user);
			return new ResponseEntity<Object>("Updated Successfully", HttpStatus.OK);
		} else {
			return ResponseEntity.notFound().build();
		}
	}

	// edit percobaan masuk
	@PutMapping("edit/loginattempt/{id}")
	public ResponseEntity<Object> editLoginAttempt(@PathVariable("id") Long id, @RequestBody User user) {
		Optional<User> userData = this.userRepository.findById(id);
		if (userData.isPresent()) {
			int temp = userData.get().getLoginAttempt() + 1;
			if (temp > 2) {
				user.setId(userData.get().getId());
				user.setModifyOn(Date.from(Instant.now()));
				user.setCreatedBy(userData.get().getCreatedBy());
				user.setCreatedOn(userData.get().getCreatedOn());
				user.setBiodataId(userData.get().getBiodataId());
				user.setRoleId(userData.get().getRoleId());
				user.setPassword(userData.get().getPassword());
				user.setDeletedBy(userData.get().getDeletedBy());
				user.setDeletedOn(userData.get().getDeletedOn());
				user.setEmail(userData.get().getEmail());
				user.setIsDelete(userData.get().getIsDelete());
				user.setIsLocked(true);
				user.setLastLogin(userData.get().getLastLogin());
				user.setLoginAttempt(3);
			} else {
				user.setId(userData.get().getId());
				user.setModifyOn(Date.from(Instant.now()));
				user.setCreatedBy(userData.get().getCreatedBy());
				user.setCreatedOn(userData.get().getCreatedOn());
				user.setBiodataId(userData.get().getBiodataId());
				user.setRoleId(userData.get().getRoleId());
				user.setPassword(userData.get().getPassword());
				user.setDeletedBy(userData.get().getDeletedBy());
				user.setDeletedOn(userData.get().getDeletedOn());
				user.setEmail(userData.get().getEmail());
				user.setIsDelete(userData.get().getIsDelete());
				user.setIsLocked(userData.get().getIsLocked());
				user.setLastLogin(userData.get().getLastLogin());
			}
			this.userRepository.save(user);
			return new ResponseEntity<Object>("Updated Successfully", HttpStatus.OK);
		} else {
			return ResponseEntity.notFound().build();
		}
	}

	// post mapping untuk modal daftar pada backlog Pendaftaran Akun
	@PostMapping("add/user")
	public ResponseEntity<Object> saveUser(@RequestBody User user) {
		user.setCreatedOn(Date.from(Instant.now()));
		user.setIsDelete(false);
		user.setIsLocked(false);
		user.setLoginAttempt(0);

		User userData = this.userRepository.save(user);

		if (userData.equals(user)) {
			return new ResponseEntity<Object>("Save Data Successfully", HttpStatus.OK);
		} else {
			return new ResponseEntity<Object>("Save Failed", HttpStatus.BAD_REQUEST);
		}
	}

	// get max user id
	@GetMapping("usermaxid")
	public ResponseEntity<Long> getMaxUserID() {
		try {
			Long userID = this.userRepository.findByMaxId();
			return new ResponseEntity<Long>(userID, HttpStatus.OK);
		} catch (Exception e) {
			return new ResponseEntity<Long>(HttpStatus.NO_CONTENT);
		}
	}
	
	
	//put mapping untuk createdBy dan biodata Id
	@PutMapping("edit/bioIdcreatedBy/{id}")
	public ResponseEntity<Object> editCreatedBy(@PathVariable("id") Long id, @RequestBody User user) {
		Optional<User> userData = this.userRepository.findById(id);
		if (userData.isPresent()) {
			user.setId(userData.get().getId());
			user.setEmail(userData.get().getEmail());
			user.setPassword(userData.get().getPassword());

			user.setCreatedOn(userData.get().getCreatedOn());
			user.setRoleId(userData.get().getRoleId());
			user.setDeletedBy(userData.get().getDeletedBy());
			user.setDeletedOn(userData.get().getDeletedOn());
			user.setIsDelete(userData.get().getIsDelete());
			user.setIsLocked(userData.get().getIsLocked());
			user.setLastLogin(Date.from(Instant.now()));
			user.setLoginAttempt(userData.get().getLoginAttempt());
			user.setLastLogin(userData.get().getLastLogin());
			
			this.userRepository.save(user);
			return new ResponseEntity<Object>("Updated Successfully", HttpStatus.OK);
		} else {
			return ResponseEntity.notFound().build();
		}
	}

	// untuk mengubah password
	@PutMapping("edit/userpassword/{id}")
	public ResponseEntity<Object> editUserPassword(@PathVariable("id") Long id, @RequestBody User user) {
		Optional<User> userData = this.userRepository.findById(id);
		if (userData.isPresent()) {
			user.setId(userData.get().getId());
			user.setEmail(userData.get().getEmail());
			user.setModifyOn(Date.from(Instant.now()));
			user.setLastLogin(userData.get().getLastLogin());

			user.setCreatedBy(userData.get().getCreatedBy());
			user.setCreatedOn(userData.get().getCreatedOn());
			user.setBiodataId(userData.get().getBiodataId());
			user.setRoleId(userData.get().getRoleId());
			user.setDeletedBy(userData.get().getDeletedBy());
			user.setDeletedOn(userData.get().getDeletedOn());
			user.setIsDelete(userData.get().getIsDelete());
			user.setIsLocked(false);
			user.setLoginAttempt(0);

			this.userRepository.save(user);
			return new ResponseEntity<Object>("Updated Successfully", HttpStatus.OK);
		} else {
			return ResponseEntity.notFound().build();
		}
	}
	
	@GetMapping("hashpassword/")
	public static String MD5(@RequestParam("password") String s) throws Exception {
		      MessageDigest m=MessageDigest.getInstance("MD5");
		      m.update(s.getBytes(),0,s.length());     
		      return new BigInteger(1,m.digest()).toString(16); 
	}

}
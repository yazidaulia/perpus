package com.perpustakaan.web.controller;

import java.time.Instant;
import java.util.Date;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.perpustakaan.web.model.Biodata;
import com.perpustakaan.web.repository.BiodataRepository;

@RestController
@CrossOrigin("*")
@RequestMapping("/api/")
public class ApiBiodataController {

	@Autowired
	public BiodataRepository biodataRepository;

	@PostMapping("add/biodata")
	public ResponseEntity<Object> saveBiodata(@RequestBody Biodata biodata) {
		biodata.setCreatedOn(Date.from(Instant.now()));
		biodata.setIsDelete(false);

		Biodata biodataData = this.biodataRepository.save(biodata);

		if (biodataData.equals(biodata)) {
			return new ResponseEntity<Object>("Save Data Successfully", HttpStatus.OK);
		} else {
			return new ResponseEntity<Object>("Save Failed", HttpStatus.BAD_REQUEST);
		}
	}

	// get biodata id with createdBy
	@GetMapping("getBioId/")
	public ResponseEntity<List<Biodata>> getBioId(@RequestParam("createdBy") Long createdBy) {
		try {
			List<Biodata> biodataId = this.biodataRepository.findByCreatedBy(createdBy);
			return new ResponseEntity<List<Biodata>>(biodataId, HttpStatus.OK);
		} catch (Exception e) {
			return new ResponseEntity<List<Biodata>>(HttpStatus.NO_CONTENT);
		}
	}

	//get max biodata id
	@GetMapping("biodatamaxid")
	public ResponseEntity<Long> getMaxBiodataID() {
		try {
			Long biodataID = this.biodataRepository.findByMaxId();
			return new ResponseEntity<Long>(biodataID, HttpStatus.OK);
		} catch (Exception e) {
			return new ResponseEntity<Long>(HttpStatus.NO_CONTENT);
		}
	}

	@PutMapping("edit/biodata/{id}")
	public ResponseEntity<Object> editBiodata(@PathVariable("id") Long id, @RequestBody Biodata biodata) {
		Optional<Biodata> biodataData = this.biodataRepository.findById(id);
		if (biodataData.isPresent()) {
			biodata.setId(id);
			biodata.setFullName(biodataData.get().getFullName());
			biodata.setModifyOn(Date.from(Instant.now()));
			biodata.setCreatedOn(biodataData.get().getCreatedOn());
			biodata.setMobilePhone(biodataData.get().getMobilePhone());
			biodata.setIsDelete(biodataData.get().getIsDelete());

			this.biodataRepository.save(biodata);
			return new ResponseEntity<Object>("Updated Successfully", HttpStatus.OK);
		} else {
			return ResponseEntity.notFound().build();
		}
	}
}

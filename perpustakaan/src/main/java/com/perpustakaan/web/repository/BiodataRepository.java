package com.perpustakaan.web.repository;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import com.perpustakaan.web.model.Biodata;

import java.util.List;


public interface BiodataRepository extends JpaRepository<Biodata, Long> {
	@Query("SELECT MAX(id) FROM Biodata")
	public Long findByMaxId();
	
	List<Biodata> findByCreatedBy(Long createdBy);
}
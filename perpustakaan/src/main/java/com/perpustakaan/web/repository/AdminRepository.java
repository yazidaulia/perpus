package com.perpustakaan.web.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.perpustakaan.web.model.Admin;


public interface AdminRepository extends JpaRepository<Admin, Long>{

}

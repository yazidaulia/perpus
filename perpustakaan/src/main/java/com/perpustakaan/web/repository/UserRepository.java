package com.perpustakaan.web.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import com.perpustakaan.web.model.User;


public interface UserRepository extends JpaRepository<User, Long> {
	List<User> findByIsLocked(Boolean isLocked);
	List<User> findByEmail(String email);
	@Query("SELECT MAX(id) FROM User")
	public Long findByMaxId();
}

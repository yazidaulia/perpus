package com.perpustakaan.web.repository;

import java.util.List;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import com.perpustakaan.web.model.Genre;

public interface GenreRepository extends JpaRepository<Genre, Long> {
	List<Genre> findByIsDeleteOrderByGenreNameAsc(Boolean isDelete);
	
	List<Genre> findByGenreName(String genreName);
	List<Genre> findByGenreCode(String genreCode);
	
	@Query("FROM Genre WHERE lower(genreName) LIKE lower(concat('%', ?1, '%')) AND isDelete=false")
	List<Genre> searchByKeyword(String keyword);
	
	@Query("SELECT v FROM Genre v WHERE LOWER(genreName) LIKE LOWER(CONCAT('%', ?1, '%')) AND isDelete=false")
	Page<Genre> searchByGenreName(String genreName, Pageable pageable);
}

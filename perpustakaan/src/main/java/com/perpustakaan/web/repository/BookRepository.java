package com.perpustakaan.web.repository;

import java.util.List;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import com.perpustakaan.web.model.Book;

public interface BookRepository extends JpaRepository<Book, Long> {
	List<Book> findByIsDelete(Boolean isDelete);

	@Query("FROM Book WHERE lower(bookName) LIKE lower(concat('%', ?1, '%')) AND isDelete=false")
	List<Book> searchByKeyword(String keyword);
	
	List<Book> findByBookName(String bookName);
	List<Book> findByBookCode(String bookCode);
}

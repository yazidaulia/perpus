package com.perpustakaan.web.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import com.perpustakaan.web.model.Token;


public interface TokenRepository extends JpaRepository<Token, Long>{
	List<Token> findByToken(String token);
	
	List<Token> findByIsExpired(Boolean isExpired);
}

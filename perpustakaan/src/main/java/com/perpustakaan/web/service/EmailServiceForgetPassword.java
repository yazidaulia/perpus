package com.perpustakaan.web.service;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Lazy;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.stereotype.Service;

import com.perpustakaan.web.model.Token;

@Lazy(value = true)
@Configuration
@Service
public class EmailServiceForgetPassword {

	@Autowired
	public JavaMailSender javaMailSender;

	@Value("${spring.mail.username}")
	private String sender;

	public String sendSimpleMail(Token token) {

		try {

			// Creating a simple mail message
			SimpleMailMessage mailMessage = new SimpleMailMessage();

			// Setting up necessary details
			mailMessage.setFrom(sender);
			mailMessage.setTo(token.getEmail());
			mailMessage.setText(templateSimpleMessage().getText()+"Kode OTP reset password mu\n"+token.getToken());
			mailMessage.setSubject("OTP Forget Password");

			// Sending the mail
			javaMailSender.send(mailMessage);
			return "Mail Sent Successfully...";
		}

		catch (Exception e) {
			return "Error while Sending Mail";
		}
	}
	
	//set body email
	@Bean
	public SimpleMailMessage templateSimpleMessage() {
		SimpleMailMessage message = new SimpleMailMessage();
		
		message.setText("JANGAN PERNAH MEMBERITAHUKAN OTP INI KEPADA SIAPAPUN \n\n");
		return message;
	}

}
